#include "minako.h"
#include <stdio.h>
#include <stdlib.h>

int currentToken;
int nextToken;

void eat() {
  currentToken = nextToken;
  nextToken = yylex();
}

int isToken(int token) {
  return (token == currentToken);
}

void isTokenAndEat(int token) {
  if(!isToken(token)) {
    //fprintf(stderr,"ERROR: Syntaxfehler in Zeile %d\n", yylineno);
    fprintf(stderr,"ERROR: Syntaxfehler in Zeile %d (expected %d)\n", yylineno, token);
    exit(EXIT_FAILURE);
  }
  eat();
}

/* C1 grammar starts here */
void type() {
  if(isToken(KW_BOOLEAN) ||
      isToken(KW_FLOAT) ||
      isToken(KW_INT) ||
      isToken(KW_VOID)) {
    eat();
  } else {
    isTokenAndEat(-2);
  }
}

void statement();
void statementlist();
void block() {
  if(isToken('{')) {
    isTokenAndEat('{');
    statementlist();
    isTokenAndEat('}');
  } else {
    statement();
  }
}

void functioncall();
void assignment();
void factor() {
  if(isToken(CONST_INT) || isToken(CONST_FLOAT) || isToken(CONST_BOOLEAN)) {
    eat();
    return;
  }
  if(isToken(ID)) {
    if(nextToken=='(') {
      functioncall();
      return;
    }
    eat();
    return;
  }
  if(isToken('(')) {
    eat();
    assignment();
    isTokenAndEat(')');
  }
}

void term() {
  factor();
  while(isToken('*') || isToken('/') || isToken(AND)) {
    eat();
    factor();
  }
}

void simpexpr() {
  if(isToken('-')) {
    eat();
  }
  term();
  while(isToken('+') || isToken('-') || isToken(OR)) {
    eat();
    term();
  }
}

void expr() {
  simpexpr();
  if(isToken(EQ) ||
      isToken(NEQ) ||
      isToken(LEQ) ||
      isToken(GEQ) ||
      isToken(LSS) ||
      isToken(GRT)) {
    eat();
    simpexpr();
  }
}

void assignment() {
  if(isToken(ID) && nextToken=='=') {
    eat();eat();
    assignment();
  } else {
    expr();
  }
}

void ifstatement() {
  isTokenAndEat(KW_IF);
  isTokenAndEat('(');
  assignment();
  isTokenAndEat(')');
  block();
}

void returnstatement() {
  isTokenAndEat(KW_RETURN);
  if(isToken(ID) ||
      isToken('-') ||
      isToken(CONST_INT) ||
      isToken(CONST_FLOAT) ||
      isToken(CONST_BOOLEAN) ||
      isToken('(')) {
    assignment();
  }
}

void c1_printf() {
  isTokenAndEat(KW_PRINTF);
  isTokenAndEat('(');
  assignment();
  isTokenAndEat(')');
}

void statassignment() {
  isTokenAndEat(ID);
  isTokenAndEat('=');
  assignment();
}

void functioncall() {
  isTokenAndEat(ID);
  isTokenAndEat('(');
  isTokenAndEat(')');
}

void statement() {
  if(isToken(KW_IF)) {
    ifstatement();
    return;
  }
  if(isToken(KW_RETURN)) {
    returnstatement();
    isTokenAndEat(';');
    return;
  }
  if(isToken(KW_PRINTF)) {
    c1_printf();
    isTokenAndEat(';');
    return;
  }
  if(isToken(ID)) {
    if(nextToken == '(') {
      functioncall();
      isTokenAndEat(';');
      return;
    }
    if(nextToken == '=') {
      statassignment();
      isTokenAndEat(';');
      return;
    }
  }
}

void statementlist() {
  while(isToken('{') ||
      isToken(KW_IF) ||
      isToken(KW_RETURN) ||
      isToken(KW_PRINTF) ||
      isToken(ID)) {
    block();
  }
}

void functiondefinition() {
  type();
  isTokenAndEat(ID);
  isTokenAndEat('(');
  isTokenAndEat(')');
  isTokenAndEat('{');
  statementlist();
  isTokenAndEat('}');
}
void program() {
  while(!isToken(EOF)) {
    functiondefinition();
  }
}

int main(int argc, char** argv) {

  FILE* input;
  if(argc<2) {
    input = stdin;
  } else {
    input = fopen(argv[1],"r");
  }
  yyin = input;

  // start parsing
  currentToken=yylex();
  nextToken=yylex();
  program();

  if(input!=stdin) fclose(input);
  return EXIT_SUCCESS;
}
